import babel from 'rollup-plugin-babel';
// import commonjs from 'rollup-plugin-commonjs';
// import resolve from 'rollup-plugin-node-resolve';
import uglify from 'rollup-plugin-uglify';

const isDevelopment = process.env.NODE_ENV !== 'production'
const shouldMinify  = process.env.MINIFY === 'true'

const distDir = isDevelopment ? '.tmp' : 'dist'

export default {
  input: 'src/index.js',

  output: {
    file: `${distDir}/veil${shouldMinify ? '.min': ''}.js`,
    format: 'iife',
    name: 'Veil',
    sourcemap: isDevelopment
  },

  plugins: [
    babel({
      exclude: 'node_modules/**',
      runtimeHelpers: true
    }),

    shouldMinify
      ? uglify({ compress: { drop_console: true } })
      : {}
  ]
}