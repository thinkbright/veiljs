var asyncGenerator = function () {
  function AwaitValue(value) {
    this.value = value;
  }

  function AsyncGenerator(gen) {
    var front, back;

    function send(key, arg) {
      return new Promise(function (resolve, reject) {
        var request = {
          key: key,
          arg: arg,
          resolve: resolve,
          reject: reject,
          next: null
        };

        if (back) {
          back = back.next = request;
        } else {
          front = back = request;
          resume(key, arg);
        }
      });
    }

    function resume(key, arg) {
      try {
        var result = gen[key](arg);
        var value = result.value;

        if (value instanceof AwaitValue) {
          Promise.resolve(value.value).then(function (arg) {
            resume("next", arg);
          }, function (arg) {
            resume("throw", arg);
          });
        } else {
          settle(result.done ? "return" : "normal", result.value);
        }
      } catch (err) {
        settle("throw", err);
      }
    }

    function settle(type, value) {
      switch (type) {
        case "return":
          front.resolve({
            value: value,
            done: true
          });
          break;

        case "throw":
          front.reject(value);
          break;

        default:
          front.resolve({
            value: value,
            done: false
          });
          break;
      }

      front = front.next;

      if (front) {
        resume(front.key, front.arg);
      } else {
        back = null;
      }
    }

    this._invoke = send;

    if (typeof gen.return !== "function") {
      this.return = undefined;
    }
  }

  if (typeof Symbol === "function" && Symbol.asyncIterator) {
    AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
      return this;
    };
  }

  AsyncGenerator.prototype.next = function (arg) {
    return this._invoke("next", arg);
  };

  AsyncGenerator.prototype.throw = function (arg) {
    return this._invoke("throw", arg);
  };

  AsyncGenerator.prototype.return = function (arg) {
    return this._invoke("return", arg);
  };

  return {
    wrap: function (fn) {
      return function () {
        return new AsyncGenerator(fn.apply(this, arguments));
      };
    },
    await: function (value) {
      return new AwaitValue(value);
    }
  };
}();

















































var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

var isFunction = function isFunction(value) {
  return typeof value === 'function';
};


var isUndefined = function isUndefined(value) {
  return typeof value === 'undefined';
};

var assert = function assert(value, check) {
  return !!check(value) || console.error('assert: ' + value + ' failed ' + check.name);
};



var expand = function expand(str) {
  var keys = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var delimiter = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ':';
  return zip(keys, str.split(delimiter));
};

var remove$1 = function remove(item, arr) {
  return arr.splice(arr.indexOf(item), 1);
};

var zip = function zip(keys, values) {
  if (!keys.length) return values;

  var result = keys.reduce(function (result, key) {
    if (values.length) result[key] = values.shift();
    return result;
  }, {});

  if (values.length) result._rest = values;

  return result;
};

var extend = !isFunction(Object.assign) ? function (target) {
  for (var _len = arguments.length, sources = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    sources[_key - 1] = arguments[_key];
  }

  return sources.reduceRight(function (result, source) {
    Object.keys(source).forEach(function (key) {
      result[key] = source[key];
    });

    return result;
  }, target);
} : Object.assign;

var supportsPassive = false;
try {
  var options = Object.defineProperty({}, 'passive', {
    get: function get() {
      supportsPassive = true;
    }
  });

  window.addEventListener('passive-test', null, options);
} catch (err) {}

var values = !isFunction(Object.values) ? function (obj) {
  return Object.keys(obj).map(function (key) {
    return obj[key];
  });
} : Object.values;

function attr(node, name, value) {
  return isUndefined(value) ? node.getAttribute(name) : node.setAttribute(name, value);
}

function query(query) {
  var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

  return [].concat(toConsumableArray(context.querySelectorAll(query)));
}

function queryAttr(name, context) {
  return query('[' + name.replace(':', '\\:') + ']', context);
}

function hasNode(n, nodes) {
  return nodes.some(function (node) {
    return n === node || node.contains(n);
  });
}

function mixinController(vm) {
  var name = vm.$options.namespace + ':' + vm.$name;
  var events = queryAttr(name).map(function (node) {
    return addEvent(name, node, vm);
  }).reduce(function (events, evts) {
    return events.concat(evts);
  }, []);

  if (isFunction(vm.$controller.initEvent)) vm.$on('initEvent', vm.$controller.initEvent);

  vm.$on('mount', function (node) {
    return attr(node, name) && addEvent(name, node, vm);
  });
  vm.$on('unmount', function (nodes, vm) {
    events.filter(function (evt) {
      return hasNode(evt.node, nodes);
    }).forEach(function (evt) {
      return removeEvent(evt, events);
    });
  });
  vm.$on('destroy', function (vm) {
    return events.forEach(function (evt) {
      return removeEvent(evt, events);
    });
  });

  vm.$on('init', function (vm) {
    return events.forEach(function (evt) {
      return vm.$emit('initEvent', evt);
    });
  });
}

function addEvent(name, node, vm) {
  var controllers = attr(node, name).split(',').map(function (controllerStr) {
    return controllerStr.trim();
  });
  var evts = [];

  controllers.forEach(function (controller) {
    var _expand = expand(controller, ['event', 'ref', 'capture', 'passive']),
        event = _expand.event,
        ref = _expand.ref,
        capture = _expand.capture,
        passive = _expand.passive;

    assert(vm.$controller[ref], isFunction);

    var evt = {
      event: event,
      node: node,
      cb: function cb(event) {
        return vm.$controller[ref].call(vm, node, event);
      },
      options: supportsPassive ? { capture: capture, passive: passive } : capture
    };

    evt.node.addEventListener(evt.event, evt.cb, evt.options);

    evts.push(evt);
  });

  return evts;
}

function removeEvent(evt, events) {
  evt.node.removeEventListener(evt.event, evt.cb, evt.options);
  remove$1(evt, events);
}

export { mixinController };
