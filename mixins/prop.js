var asyncGenerator = function () {
  function AwaitValue(value) {
    this.value = value;
  }

  function AsyncGenerator(gen) {
    var front, back;

    function send(key, arg) {
      return new Promise(function (resolve, reject) {
        var request = {
          key: key,
          arg: arg,
          resolve: resolve,
          reject: reject,
          next: null
        };

        if (back) {
          back = back.next = request;
        } else {
          front = back = request;
          resume(key, arg);
        }
      });
    }

    function resume(key, arg) {
      try {
        var result = gen[key](arg);
        var value = result.value;

        if (value instanceof AwaitValue) {
          Promise.resolve(value.value).then(function (arg) {
            resume("next", arg);
          }, function (arg) {
            resume("throw", arg);
          });
        } else {
          settle(result.done ? "return" : "normal", result.value);
        }
      } catch (err) {
        settle("throw", err);
      }
    }

    function settle(type, value) {
      switch (type) {
        case "return":
          front.resolve({
            value: value,
            done: true
          });
          break;

        case "throw":
          front.reject(value);
          break;

        default:
          front.resolve({
            value: value,
            done: false
          });
          break;
      }

      front = front.next;

      if (front) {
        resume(front.key, front.arg);
      } else {
        back = null;
      }
    }

    this._invoke = send;

    if (typeof gen.return !== "function") {
      this.return = undefined;
    }
  }

  if (typeof Symbol === "function" && Symbol.asyncIterator) {
    AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
      return this;
    };
  }

  AsyncGenerator.prototype.next = function (arg) {
    return this._invoke("next", arg);
  };

  AsyncGenerator.prototype.throw = function (arg) {
    return this._invoke("throw", arg);
  };

  AsyncGenerator.prototype.return = function (arg) {
    return this._invoke("return", arg);
  };

  return {
    wrap: function (fn) {
      return function () {
        return new AsyncGenerator(fn.apply(this, arguments));
      };
    },
    await: function (value) {
      return new AwaitValue(value);
    }
  };
}();

















































var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

var isFunction = function isFunction(value) {
  return typeof value === 'function';
};


var isUndefined = function isUndefined(value) {
  return typeof value === 'undefined';
};





var expand = function expand(str) {
  var keys = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var delimiter = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ':';
  return zip(keys, str.split(delimiter));
};

var remove$1 = function remove(item, arr) {
  return arr.splice(arr.indexOf(item), 1);
};

var zip = function zip(keys, values) {
  if (!keys.length) return values;

  var result = keys.reduce(function (result, key) {
    if (values.length) result[key] = values.shift();
    return result;
  }, {});

  if (values.length) result._rest = values;

  return result;
};

var extend = !isFunction(Object.assign) ? function (target) {
  for (var _len = arguments.length, sources = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    sources[_key - 1] = arguments[_key];
  }

  return sources.reduceRight(function (result, source) {
    Object.keys(source).forEach(function (key) {
      result[key] = source[key];
    });

    return result;
  }, target);
} : Object.assign;


try {
  var options = Object.defineProperty({}, 'passive', {
    get: function get() {
      
    }
  });

  window.addEventListener('passive-test', null, options);
} catch (err) {}

var values = !isFunction(Object.values) ? function (obj) {
  return Object.keys(obj).map(function (key) {
    return obj[key];
  });
} : Object.values;

function attr(node, name, value) {
  return isUndefined(value) ? node.getAttribute(name) : node.setAttribute(name, value);
}

function query(query) {
  var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

  return [].concat(toConsumableArray(context.querySelectorAll(query)));
}

function queryAttr(name, context) {
  return query('[' + name.replace(':', '\\:') + ']', context);
}

function hasNode(n, nodes) {
  return nodes.some(function (node) {
    return n === node || node.contains(n);
  });
}

var FILTERS = {
  currency: function currency(value) {
    var locale = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : navigator.language || navigator.userLanguage;

    var _currency = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'EUR';

    return !isNaN(parseFloat(value)) && parseFloat(value).toLocaleString(locale, {
      currency: _currency,
      style: 'currency'
    });
  },

  length: function length(value) {
    return typeof value !== 'undefined' && value.length;
  }
};

var parseFilter = function parseFilter(value, filter) {
  console.debug('parseFilter', value, filter);

  var parts = filter.split(':');
  var name = parts.shift();

  return name in FILTERS ? FILTERS[name].apply(FILTERS, [value].concat(toConsumableArray(parts))) : value && console.warn('Filter ' + name + ' is not defined.');
};





var parseProp = function parseProp(prop, data) {
  console.debug('parseProp', prop, data);

  if (prop === '') return data;

  var parts = prop.split('|');
  var result = parts.shift().toString().trim().replace(/\[(\d+?)\]/g, '.$1').split('.').reduce(function (obj, key) {
    return isFunction(obj[key]) ? obj[key]() : obj[key];
  }, data);
  var filter = typeof parts[0] === 'string' && parts.shift().trim();

  return filter ? parseFilter(result, filter) : result;
};

function mixinProp(vm) {
  var name = vm.$options.namespace + '-prop:' + vm.$name;
  var props = queryAttr(name);

  if (isFunction(vm.$controller.initProp)) vm.$on('initProp', vm.$controller.initProp);

  function updateProps(data) {
    props.forEach(function (node) {
      var _expand = expand(attr(node, name), ['prop', 'ref']),
          prop = _expand.prop,
          ref = _expand.ref;

      var value = parseProp(ref, data);

      if (!isUndefined(value) && value !== node[prop]) node[prop] = value;
    });
  }

  vm.$on('mount', function (node) {
    return attr(node, name) && props.push(node);
  });
  vm.$on('update', updateProps.bind(null, vm.$state));
  vm.$on('updateProps', updateProps);
  vm.$on('unmount', function (nodes, vm) {
    props.filter(function (node) {
      return hasNode(node, nodes);
    }).forEach(function (node) {
      return remove$1(node, props);
    });
  });

  vm.$on('init', function (vm) {
    return props.forEach(function (node) {
      return vm.$emit('initProp', node);
    });
  });
}

export { mixinProp };
