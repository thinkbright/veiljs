var Veil = (function () {
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};





var asyncGenerator = function () {
  function AwaitValue(value) {
    this.value = value;
  }

  function AsyncGenerator(gen) {
    var front, back;

    function send(key, arg) {
      return new Promise(function (resolve, reject) {
        var request = {
          key: key,
          arg: arg,
          resolve: resolve,
          reject: reject,
          next: null
        };

        if (back) {
          back = back.next = request;
        } else {
          front = back = request;
          resume(key, arg);
        }
      });
    }

    function resume(key, arg) {
      try {
        var result = gen[key](arg);
        var value = result.value;

        if (value instanceof AwaitValue) {
          Promise.resolve(value.value).then(function (arg) {
            resume("next", arg);
          }, function (arg) {
            resume("throw", arg);
          });
        } else {
          settle(result.done ? "return" : "normal", result.value);
        }
      } catch (err) {
        settle("throw", err);
      }
    }

    function settle(type, value) {
      switch (type) {
        case "return":
          front.resolve({
            value: value,
            done: true
          });
          break;

        case "throw":
          front.reject(value);
          break;

        default:
          front.resolve({
            value: value,
            done: false
          });
          break;
      }

      front = front.next;

      if (front) {
        resume(front.key, front.arg);
      } else {
        back = null;
      }
    }

    this._invoke = send;

    if (typeof gen.return !== "function") {
      this.return = undefined;
    }
  }

  if (typeof Symbol === "function" && Symbol.asyncIterator) {
    AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
      return this;
    };
  }

  AsyncGenerator.prototype.next = function (arg) {
    return this._invoke("next", arg);
  };

  AsyncGenerator.prototype.throw = function (arg) {
    return this._invoke("throw", arg);
  };

  AsyncGenerator.prototype.return = function (arg) {
    return this._invoke("return", arg);
  };

  return {
    wrap: function (fn) {
      return function () {
        return new AsyncGenerator(fn.apply(this, arguments));
      };
    },
    await: function (value) {
      return new AwaitValue(value);
    }
  };
}();

















































var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

var isArray = Array.isArray;
var isFunction = function isFunction(value) {
  return typeof value === 'function';
};
var isObject = function isObject(value) {
  return (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && value !== null;
};
var isString = function isString(value) {
  return typeof value === 'string';
};
var isUndefined = function isUndefined(value) {
  return typeof value === 'undefined';
};

var assert = function assert(value, check) {
  return !!check(value) || console.error('assert: ' + value + ' failed ' + check.name);
};



var expand = function expand(str) {
  var keys = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var delimiter = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ':';
  return zip(keys, str.split(delimiter));
};

var remove$1 = function remove(item, arr) {
  return arr.splice(arr.indexOf(item), 1);
};

var zip = function zip(keys, values) {
  if (!keys.length) return values;

  var result = keys.reduce(function (result, key) {
    if (values.length) result[key] = values.shift();
    return result;
  }, {});

  if (values.length) result._rest = values;

  return result;
};

var extend = !isFunction(Object.assign) ? function (target) {
  for (var _len = arguments.length, sources = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    sources[_key - 1] = arguments[_key];
  }

  return sources.reduceRight(function (result, source) {
    Object.keys(source).forEach(function (key) {
      result[key] = source[key];
    });

    return result;
  }, target);
} : Object.assign;

var supportsPassive = false;
try {
  var options = Object.defineProperty({}, 'passive', {
    get: function get() {
      supportsPassive = true;
    }
  });

  window.addEventListener('passive-test', null, options);
} catch (err) {}

var values = !isFunction(Object.values) ? function (obj) {
  return Object.keys(obj).map(function (key) {
    return obj[key];
  });
} : Object.values;

function VeilDOM(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    $mount: $mount,
    $unmount: $unmount
  });

  Veil = extend(Veil, {
    attr: attr,
    query: query,
    queryAttr: queryAttr,
    remove: remove
  });
}

function $mount(node, target) {
  var vm = this;

  vm.$emit('mount', node, vm);
  target.appendChild(node);
}

function $unmount(nodes) {
  var vm = this;

  vm.$emit('unmount', nodes, vm);
  Veil.remove(nodes);
}

function attr(node, name, value) {
  return isUndefined(value) ? node.getAttribute(name) : node.setAttribute(name, value);
}

function query(query) {
  var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

  return [].concat(toConsumableArray(context.querySelectorAll(query)));
}

function queryAttr(name, context) {
  return query('[' + name.replace(':', '\\:') + ']', context);
}

function hasNode(n, nodes) {
  return nodes.some(function (node) {
    return n === node || node.contains(n);
  });
}

function remove(nodes) {
  (isArray(nodes) ? nodes : [nodes]).forEach(function (node) {
    return node.parentNode.removeChild(node);
  });
}

function initEvents(vm) {
  vm._events = Object.create(null);
}

function destroyEvents(vm) {
  vm.$off();
}

function VeilEvents(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    $emit: $emit,
    $on: $on,
    $once: $once,
    $off: $off
  });
}

function $on(event, fn) {
  var vm = this;

  if (isArray(event)) event.forEach(function (event) {
    return vm.$on(event, fn);
  });else (vm._events[event] || (vm._events[event] = [])).push(fn);

  return vm;
}

function $once(event, fn) {
  var vm = this;

  function cb() {
    vm.$off(event, cb);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    fn.apply(vm, args);
  }

  cb.fn = fn;
  vm.$on(event, cb);

  return vm;
}

function $off(event, fn) {
  var vm = this;

  if (isUndefined(event) && isUndefined(fn)) initEvents(vm);

  if (isArray(event)) event.forEach(function (event) {
    return vm.$off(event, fn);
  });

  if (!fn) vm._events[event] = null;

  if (isFunction(fn)) {
    // TODO: research why we need to use a while loop to destruct events.
    // Using a foreach to remove events somehow mangles the array so that
    // not all events are properly handled.
    var cb = void 0,
        cbs = vm._events,
        i = vm._events.length;

    while (i--) {
      if (function (cb) {
        return cb === fn || cb.fn === fn;
      }) {
        cbs.splice(i, 1);
        break;
      }
    }
  }

  return vm;
}

function $emit(event) {
  for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
    args[_key2 - 1] = arguments[_key2];
  }

  var vm = this;

  vm._events[event] && vm._events[event].forEach(function (cb) {
    return cb.apply(vm, args);
  });

  return vm;
}

var FILTERS = {
  currency: function currency(value) {
    var locale = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : navigator.language || navigator.userLanguage;

    var _currency = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'EUR';

    return !isNaN(parseFloat(value)) && parseFloat(value).toLocaleString(locale, {
      currency: _currency,
      style: 'currency'
    });
  },

  length: function length(value) {
    return typeof value !== 'undefined' && value.length;
  }
};

var parseFilter = function parseFilter(value, filter) {
  console.debug('parseFilter', value, filter);

  var parts = filter.split(':');
  var name = parts.shift();

  return name in FILTERS ? FILTERS[name].apply(FILTERS, [value].concat(toConsumableArray(parts))) : value && console.warn('Filter ' + name + ' is not defined.');
};





var parseProp = function parseProp(prop, data) {
  console.debug('parseProp', prop, data);

  if (prop === '') return data;

  var parts = prop.split('|');
  var result = parts.shift().toString().trim().replace(/\[(\d+?)\]/g, '.$1').split('.').reduce(function (obj, key) {
    return isFunction(obj[key]) ? obj[key]() : obj[key];
  }, data);
  var filter = typeof parts[0] === 'string' && parts.shift().trim();

  return filter ? parseFilter(result, filter) : result;
};

function mixinAttr(vm) {
  var name = vm.$options.namespace + '-attr:' + vm.$name;
  var attrs = queryAttr(name);

  if (isFunction(vm.$controller.initAttr)) vm.$on('initAttr', vm.$controller.initAttr);

  function updateAttrs(data) {
    attrs.forEach(function (node) {
      var _expand = expand(attr(node, name), ['prop', 'ref']),
          prop = _expand.prop,
          ref = _expand.ref;

      var value = parseProp(ref, data);

      isUndefined(value) ? node.removeAttribute(prop) : attr(node, prop, value);
    });
  }

  vm.$on('mount', function (node) {
    return attr(node, name) && attrs.push(node);
  });
  vm.$on('update', updateAttrs.bind(null, vm.$state));
  vm.$on('updateAttrs', updateAttrs);
  vm.$on('unmount', function (nodes, vm) {
    attrs.filter(function (node) {
      return hasNode(node, nodes);
    }).forEach(function (node) {
      return remove$1(node, attrs);
    });
  });

  vm.$on('init', function (vm) {
    return attrs.forEach(function (node) {
      return vm.$emit('initAttr', node);
    });
  });
}

function mixinController(vm) {
  var name = vm.$options.namespace + ':' + vm.$name;
  var events = queryAttr(name).map(function (node) {
    return addEvent(name, node, vm);
  }).reduce(function (events, evts) {
    return events.concat(evts);
  }, []);

  if (isFunction(vm.$controller.initEvent)) vm.$on('initEvent', vm.$controller.initEvent);

  vm.$on('mount', function (node) {
    return attr(node, name) && addEvent(name, node, vm);
  });
  vm.$on('unmount', function (nodes, vm) {
    events.filter(function (evt) {
      return hasNode(evt.node, nodes);
    }).forEach(function (evt) {
      return removeEvent(evt, events);
    });
  });
  vm.$on('destroy', function (vm) {
    return events.forEach(function (evt) {
      return removeEvent(evt, events);
    });
  });

  vm.$on('init', function (vm) {
    return events.forEach(function (evt) {
      return vm.$emit('initEvent', evt);
    });
  });
}

function addEvent(name, node, vm) {
  var controllers = attr(node, name).split(',').map(function (controllerStr) {
    return controllerStr.trim();
  });
  var evts = [];

  controllers.forEach(function (controller) {
    var _expand = expand(controller, ['event', 'ref', 'capture', 'passive']),
        event = _expand.event,
        ref = _expand.ref,
        capture = _expand.capture,
        passive = _expand.passive;

    assert(vm.$controller[ref], isFunction);

    var evt = {
      event: event,
      node: node,
      cb: function cb(event) {
        return vm.$controller[ref].call(vm, node, event);
      },
      options: supportsPassive ? { capture: capture, passive: passive } : capture
    };

    evt.node.addEventListener(evt.event, evt.cb, evt.options);

    evts.push(evt);
  });

  return evts;
}

function removeEvent(evt, events) {
  evt.node.removeEventListener(evt.event, evt.cb, evt.options);
  remove$1(evt, events);
}

function mixinProp(vm) {
  var name = vm.$options.namespace + '-prop:' + vm.$name;
  var props = queryAttr(name);

  if (isFunction(vm.$controller.initProp)) vm.$on('initProp', vm.$controller.initProp);

  function updateProps(data) {
    props.forEach(function (node) {
      var _expand = expand(attr(node, name), ['prop', 'ref']),
          prop = _expand.prop,
          ref = _expand.ref;

      var value = parseProp(ref, data);

      if (!isUndefined(value) && value !== node[prop]) node[prop] = value;
    });
  }

  vm.$on('mount', function (node) {
    return attr(node, name) && props.push(node);
  });
  vm.$on('update', updateProps.bind(null, vm.$state));
  vm.$on('updateProps', updateProps);
  vm.$on('unmount', function (nodes, vm) {
    props.filter(function (node) {
      return hasNode(node, nodes);
    }).forEach(function (node) {
      return remove$1(node, props);
    });
  });

  vm.$on('init', function (vm) {
    return props.forEach(function (node) {
      return vm.$emit('initProp', node);
    });
  });
}



var MIXINS = Object.freeze({
	mixinAttr: mixinAttr,
	mixinController: mixinController,
	mixinProp: mixinProp
});

function initState(vm) {
  vm.$state = {};
}

function destroyState(vm) {
  initState(vm);
}

function VeilState(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    $setState: $setState
  });
}

function $setState(data) {
  var vm = this;

  vm.$state = extend(vm.$state, isFunction(data) ? data(vm.$state) : data);

  vm._update();

  return vm;
}

var isController = function isController(value) {
  return isObject(value) || isFunction(value);
};
var HOOKS = ['destroy', 'init', 'mount', 'unmount', 'update'];

var $id = 0;

function initLifecycle(vm) {
  HOOKS.forEach(function (hook) {
    isFunction(vm.$controller[hook]) && vm.$on(hook, vm.$controller[hook]);
  });
}

function VeilLifecycle(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    _destroy: _destroy,
    _init: _init,
    _update: _update
  });
}

function _init(name) {
  var vm = this;

  // Check the types of the Veil arguments
  assert(name, isString);

  for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    args[_key - 1] = arguments[_key];
  }

  var mixins = isArray(args[0]) ? args.shift() : values(MIXINS);
  var controller = assert(args[0], isController) && args.shift();
  var options = isObject(args[0]) ? args.hift() : { namespace: 'veil'

    // Increment the UID of the Veil instance
  };vm._id = $id++;
  vm.$name = name;
  vm.$options = options;

  initEvents(vm);
  initState(vm);

  vm.$controller = isFunction(controller) ? new controller(vm) : controller;

  initLifecycle(vm);

  vm.$mixin.apply(vm, toConsumableArray(mixins));
  vm.$emit('init', vm);

  return vm;
}

function _update() {
  var vm = this;

  vm.$emit('update', vm);

  return vm;
}

function _destroy() {
  var vm = this;

  vm.$emit('destroy', vm);

  destroyEvents(vm);
  destroyState(vm);

  return vm;
}

function VeilMixin(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    $mixin: $mixin
  });

  Veil = extend(Veil, MIXINS);
}

function $mixin() {
  var vm = this;

  for (var _len = arguments.length, mixins = Array(_len), _key = 0; _key < _len; _key++) {
    mixins[_key] = arguments[_key];
  }

  mixins.forEach(function (mixin) {
    return isFunction(mixin) && mixin(vm);
  });

  return vm;
}

function Veil$1() {
  var vm = this;

  vm._init.apply(vm, arguments);
  console.log(vm);
  return vm;
}

VeilDOM(Veil$1);
VeilLifecycle(Veil$1);
VeilEvents(Veil$1);
VeilState(Veil$1);
VeilMixin(Veil$1);

return Veil$1;

}());
