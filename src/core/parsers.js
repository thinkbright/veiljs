import { isFunction } from './utils'

export const FILTERS = {
  currency: (
    value,
    locale = navigator.language || navigator.userLanguage,
    currency = 'EUR'
  ) =>
    !isNaN(parseFloat(value)) &&
    parseFloat(value).toLocaleString(locale, {
      currency,
      style: 'currency'
    }),

  length: value => typeof value !== 'undefined' && value.length
}

export const parseFilter = (value, filter) => {
  console.debug(`parseFilter`, value, filter)

  const parts = filter.split(':')
  const name = parts.shift()

  return name in FILTERS
    ? FILTERS[name](value, ...parts)
    : value && console.warn(`Filter ${name} is not defined.`)
}

export const registerFilter = (name, filterFn) => {
  if (name in FILTERS)
    return console.warn(`Filter ${name} is already registered`)

  return (FILTERS[name] = filterFn)
}

export const removeFilter = name => {
  delete FILTERS[name]
}

export const parseProp = (prop, data) => {
  console.debug(`parseProp`, prop, data)

  if (prop === '') return data

  const parts = prop.split('|')
  const result = parts
    .shift()
    .toString()
    .trim()
    .replace(/\[(\d+?)\]/g, '.$1')
    .split('.')
    .reduce((obj, key) => (isFunction(obj[key]) ? obj[key]() : obj[key]), data)
  const filter = typeof parts[0] === 'string' && parts.shift().trim()

  return filter ? parseFilter(result, filter) : result
}
