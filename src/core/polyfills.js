import { isFunction } from './utils'

export const extend = !isFunction(Object.assign)
  ? (target, ...sources) => {
      return sources.reduceRight((result, source) => {
        Object.keys(source).forEach(key => {
          result[key] = source[key]
        })

        return result
      }, target)
    }
  : Object.assign

export let supportsPassive = false
try {
  const options = Object.defineProperty({}, 'passive', {
    get: function() {
      supportsPassive = true
    }
  })

  window.addEventListener('passive-test', null, options)
} catch (err) {}

export const values = !isFunction(Object.values)
  ? obj => Object.keys(obj).map(key => obj[key])
  : Object.values
