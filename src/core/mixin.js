import * as MIXINS from '../mixins/index'

import { extend } from './polyfills'
import { isFunction } from './utils'

export function VeilMixin(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    $mixin
  })

  Veil = extend(Veil, MIXINS)
}

export function $mixin(...mixins) {
  const vm = this

  mixins.forEach(mixin => isFunction(mixin) && mixin(vm))

  return vm
}
