export const isArray = Array.isArray
export const isFunction = value => typeof value === 'function'
export const isObject = value => typeof value === 'object' && value !== null
export const isString = value => typeof value === 'string'
export const isUndefined = value => typeof value === 'undefined'

export const assert = (value, check) =>
  !!check(value) || console.error(`assert: ${value} failed ${check.name}`)

export const compose = (...fns) => x => fns.reduce((val, fn) => fn(val), x)

export const expand = (str, keys = [], delimiter = ':') =>
  zip(keys, str.split(delimiter))

export const remove = (item, arr) => arr.splice(arr.indexOf(item), 1)

export const zip = (keys, values) => {
  if (!keys.length) return values

  let result = keys.reduce((result, key) => {
    if (values.length) result[key] = values.shift()
    return result
  }, {})

  if (values.length) result._rest = values

  return result
}
