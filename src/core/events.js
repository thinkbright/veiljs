import { extend } from './polyfills'
import { isArray, isFunction, isUndefined, remove } from './utils'

export function initEvents(vm) {
  vm._events = Object.create(null)
}

export function destroyEvents(vm) {
  vm.$off()
}

export function VeilEvents(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    $emit,
    $on,
    $once,
    $off
  })
}

export function $on(event, fn) {
  const vm = this

  if (isArray(event)) event.forEach(event => vm.$on(event, fn))
  else (vm._events[event] || (vm._events[event] = [])).push(fn)

  return vm
}

export function $once(event, fn) {
  const vm = this

  function cb(...args) {
    vm.$off(event, cb)
    fn.apply(vm, args)
  }

  cb.fn = fn
  vm.$on(event, cb)

  return vm
}

export function $off(event, fn) {
  const vm = this

  if (isUndefined(event) && isUndefined(fn)) initEvents(vm)

  if (isArray(event)) event.forEach(event => vm.$off(event, fn))

  if (!fn) vm._events[event] = null

  if (isFunction(fn)) {
    // TODO: research why we need to use a while loop to destruct events.
    // Using a foreach to remove events somehow mangles the array so that
    // not all events are properly handled.
    let cb,
      cbs = vm._events,
      i = vm._events.length

    while (i--) {
      cb = cbs[i]

      if (cb => cb === fn || cb.fn === fn) {
        cbs.splice(i, 1)
        break
      }
    }
  }

  return vm
}

export function $emit(event, ...args) {
  const vm = this

  vm._events[event] && vm._events[event].forEach(cb => cb.apply(vm, args))

  return vm
}
