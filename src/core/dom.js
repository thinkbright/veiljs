import { extend } from './polyfills'
import { isArray, isFunction, isUndefined } from './utils'

export function VeilDOM(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    $mount,
    $unmount
  })

  Veil = extend(Veil, {
    attr,
    query,
    queryAttr,
    remove
  })
}

export function $mount(node, target) {
  const vm = this

  vm.$emit('mount', node, vm)
  target.appendChild(node)
}

export function $unmount(nodes) {
  const vm = this

  vm.$emit('unmount', nodes, vm)
  Veil.remove(nodes)
}

export function attr(node, name, value) {
  return isUndefined(value)
    ? node.getAttribute(name)
    : node.setAttribute(name, value)
}

export function query(query, context = document) {
  return [...context.querySelectorAll(query)]
}

export function queryAttr(name, context) {
  return query(`[${name.replace(':', '\\:')}]`, context)
}

export function hasNode(n, nodes) {
  return nodes.some(node => n === node || node.contains(n))
}

export function remove(nodes) {
  ;(isArray(nodes) ? nodes : [nodes]).forEach(node =>
    node.parentNode.removeChild(node)
  )
}
