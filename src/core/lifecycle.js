import * as MIXINS from '../mixins/index'

import { initEvents, destroyEvents } from './events'
import { initState, destroyState } from './state'
import { extend, values } from './polyfills'
import { assert, isArray, isFunction, isObject, isString } from './utils'

const isController = value => isObject(value) || isFunction(value)
const HOOKS = ['destroy', 'init', 'mount', 'unmount', 'update']

let $id = 0

export function initLifecycle(vm) {
  HOOKS.forEach(hook => {
    isFunction(vm.$controller[hook]) && vm.$on(hook, vm.$controller[hook])
  })
}

export function VeilLifecycle(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    _destroy,
    _init,
    _update
  })
}

export function _init(name, ...args) {
  const vm = this

  // Check the types of the Veil arguments
  assert(name, isString)
  const mixins = isArray(args[0]) ? args.shift() : values(MIXINS)
  const controller = assert(args[0], isController) && args.shift()
  const options = isObject(args[0]) ? args.hift() : { namespace: 'veil' }

  // Increment the UID of the Veil instance
  vm._id = $id++
  vm.$name = name
  vm.$options = options

  initEvents(vm)
  initState(vm)

  vm.$controller = isFunction(controller) ? new controller(vm) : controller

  initLifecycle(vm)

  vm.$mixin(...mixins)
  vm.$emit('init', vm)

  return vm
}

export function _update() {
  const vm = this

  vm.$emit('update', vm)

  return vm
}

export function _destroy() {
  const vm = this

  vm.$emit('destroy', vm)

  destroyEvents(vm)
  destroyState(vm)

  return vm
}
