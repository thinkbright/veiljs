import { extend } from './polyfills'
import { isFunction } from './utils'

export function initState(vm) {
  vm.$state = {}
}

export function destroyState(vm) {
  initState(vm)
}

export function VeilState(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    $setState
  })
}

export function $setState(data) {
  const vm = this

  vm.$state = extend(vm.$state, isFunction(data) ? data(vm.$state) : data)

  vm._update()

  return vm
}
