import { VeilDOM } from './core/dom'
import { VeilEvents } from './core/events'
import { VeilLifecycle } from './core/lifecycle'
import { VeilMixin } from './core/mixin'
import { VeilState } from './core/state'

export default function Veil(...args) {
  const vm = this

  vm._init(...args)
  console.log(vm)
  return vm
}

VeilDOM(Veil)
VeilLifecycle(Veil)
VeilEvents(Veil)
VeilState(Veil)
VeilMixin(Veil)
