export { mixinAttr } from './attr'
export { mixinController } from './controller'
export { mixinProp } from './prop'
