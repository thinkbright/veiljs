import { attr, hasNode, queryAttr } from '../core/dom'
import { expand, isFunction, isUndefined, remove } from '../core/utils'
import { parseProp } from '../core/parsers'

export function mixinProp(vm) {
  const name = `${vm.$options.namespace}-prop:${vm.$name}`
  const props = queryAttr(name)

  if (isFunction(vm.$controller.initProp))
    vm.$on('initProp', vm.$controller.initProp)

  function updateProps(data) {
    props.forEach(node => {
      const { prop, ref } = expand(attr(node, name), ['prop', 'ref'])
      const value = parseProp(ref, data)

      if (!isUndefined(value) && value !== node[prop]) node[prop] = value
    })
  }

  vm.$on('mount', node => attr(node, name) && props.push(node))
  vm.$on('update', updateProps.bind(null, vm.$state))
  vm.$on('updateProps', updateProps)
  vm.$on('unmount', (nodes, vm) => {
    props
      .filter(node => hasNode(node, nodes))
      .forEach(node => remove(node, props))
  })

  vm.$on('init', vm => props.forEach(node => vm.$emit('initProp', node)))
}
