import { attr, hasNode, queryAttr } from '../core/dom'
import { extend, supportsPassive } from '../core/polyfills'
import { assert, expand, isFunction, remove } from '../core/utils'

export function mixinController(vm) {
  const name = `${vm.$options.namespace}:${vm.$name}`
  const events = queryAttr(name)
    .map(node => addEvent(name, node, vm))
    .reduce((events, evts) => events.concat(evts), [])

  if (isFunction(vm.$controller.initEvent))
    vm.$on('initEvent', vm.$controller.initEvent)

  vm.$on('mount', node => attr(node, name) && addEvent(name, node, vm))
  vm.$on('unmount', (nodes, vm) => {
    events
      .filter(evt => hasNode(evt.node, nodes))
      .forEach(evt => removeEvent(evt, events))
  })
  vm.$on('destroy', vm => events.forEach(evt => removeEvent(evt, events)))

  vm.$on('init', vm => events.forEach(evt => vm.$emit('initEvent', evt)))
}

function addEvent(name, node, vm) {
  const controllers = attr(node, name)
    .split(',')
    .map(controllerStr => controllerStr.trim())
  const evts = []

  controllers.forEach(controller => {
    const { event, ref, capture, passive } = expand(controller, [
      'event',
      'ref',
      'capture',
      'passive'
    ])

    assert(vm.$controller[ref], isFunction)

    const evt = {
      event,
      node,
      cb: event => vm.$controller[ref].call(vm, node, event),
      options: supportsPassive ? { capture, passive } : capture
    }

    evt.node.addEventListener(evt.event, evt.cb, evt.options)

    evts.push(evt)
  })

  return evts
}

function removeEvent(evt, events) {
  evt.node.removeEventListener(evt.event, evt.cb, evt.options)
  remove(evt, events)
}
