import { attr, hasNode, queryAttr } from '../core/dom'
import { expand, isFunction, isUndefined, remove } from '../core/utils'
import { parseProp } from '../core/parsers'

export function mixinAttr(vm) {
  const name = `${vm.$options.namespace}-attr:${vm.$name}`
  const attrs = queryAttr(name)

  if (isFunction(vm.$controller.initAttr))
    vm.$on('initAttr', vm.$controller.initAttr)

  function updateAttrs(data) {
    attrs.forEach(node => {
      const { prop, ref } = expand(attr(node, name), ['prop', 'ref'])
      const value = parseProp(ref, data)

      isUndefined(value) ? node.removeAttribute(prop) : attr(node, prop, value)
    })
  }

  vm.$on('mount', node => attr(node, name) && attrs.push(node))
  vm.$on('update', updateAttrs.bind(null, vm.$state))
  vm.$on('updateAttrs', updateAttrs)
  vm.$on('unmount', (nodes, vm) => {
    attrs
      .filter(node => hasNode(node, nodes))
      .forEach(node => remove(node, attrs))
  })

  vm.$on('init', vm => attrs.forEach(node => vm.$emit('initAttr', node)))
}
