# Veil

Thin controllers for HTML elements

## Install

```
npm install git+https://brombrommert@bitbucket.org/thinkbright/veiljs.git#v0.3.8
```

Include it in your JS poject like:

```
import Veil from 'veiljs';
```

## Usage

```
const toggle = new Veil('toggle', {
  click: function({ state }) {
    state.on = !state.on
  }
})
```

## Development

Everything is in the `src/` directory.

Run in a node container using shipmate and docker:
```
shipmate deploy development
shipmate attach node
npm install
npm start
```

Or, run locally
```
npm install
npm start
```
