const glob     = require('glob')
const rollup   = require('rollup')
const babel    = require('rollup-plugin-babel')
const uglify   = require('rollup-plugin-uglify')

const isDevelopment = process.env.NODE_ENV !== 'production'

async function build (srcFile, distFile) {
  const bundle = await rollup.rollup({
    input: srcFile,

    plugins: [
      babel({
        // TBH, I don't know whether we should ignore these... -RZ
        exclude: 'node_modules/**',
        // runtimeHelpers: true
      })
    ]
  })

  await bundle.write({
    file: distFile,
    format: 'es'
  })
}

// Build individual files for use as modules
glob('src/**/*.js', (err, files)=> {
  if (err) throw err

  files.forEach(srcFile => {
    const distFile = srcFile.replace(/^src\//g, '')

    build(srcFile, distFile)
  })
})
