var asyncGenerator = function () {
  function AwaitValue(value) {
    this.value = value;
  }

  function AsyncGenerator(gen) {
    var front, back;

    function send(key, arg) {
      return new Promise(function (resolve, reject) {
        var request = {
          key: key,
          arg: arg,
          resolve: resolve,
          reject: reject,
          next: null
        };

        if (back) {
          back = back.next = request;
        } else {
          front = back = request;
          resume(key, arg);
        }
      });
    }

    function resume(key, arg) {
      try {
        var result = gen[key](arg);
        var value = result.value;

        if (value instanceof AwaitValue) {
          Promise.resolve(value.value).then(function (arg) {
            resume("next", arg);
          }, function (arg) {
            resume("throw", arg);
          });
        } else {
          settle(result.done ? "return" : "normal", result.value);
        }
      } catch (err) {
        settle("throw", err);
      }
    }

    function settle(type, value) {
      switch (type) {
        case "return":
          front.resolve({
            value: value,
            done: true
          });
          break;

        case "throw":
          front.reject(value);
          break;

        default:
          front.resolve({
            value: value,
            done: false
          });
          break;
      }

      front = front.next;

      if (front) {
        resume(front.key, front.arg);
      } else {
        back = null;
      }
    }

    this._invoke = send;

    if (typeof gen.return !== "function") {
      this.return = undefined;
    }
  }

  if (typeof Symbol === "function" && Symbol.asyncIterator) {
    AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
      return this;
    };
  }

  AsyncGenerator.prototype.next = function (arg) {
    return this._invoke("next", arg);
  };

  AsyncGenerator.prototype.throw = function (arg) {
    return this._invoke("throw", arg);
  };

  AsyncGenerator.prototype.return = function (arg) {
    return this._invoke("return", arg);
  };

  return {
    wrap: function (fn) {
      return function () {
        return new AsyncGenerator(fn.apply(this, arguments));
      };
    },
    await: function (value) {
      return new AwaitValue(value);
    }
  };
}();

var isArray = Array.isArray;
var isFunction = function isFunction(value) {
  return typeof value === 'function';
};


var isUndefined = function isUndefined(value) {
  return typeof value === 'undefined';
};

var extend = !isFunction(Object.assign) ? function (target) {
  for (var _len = arguments.length, sources = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    sources[_key - 1] = arguments[_key];
  }

  return sources.reduceRight(function (result, source) {
    Object.keys(source).forEach(function (key) {
      result[key] = source[key];
    });

    return result;
  }, target);
} : Object.assign;


try {
  var options = Object.defineProperty({}, 'passive', {
    get: function get() {
      
    }
  });

  window.addEventListener('passive-test', null, options);
} catch (err) {}

var values = !isFunction(Object.values) ? function (obj) {
  return Object.keys(obj).map(function (key) {
    return obj[key];
  });
} : Object.values;

function initEvents(vm) {
  vm._events = Object.create(null);
}

function destroyEvents(vm) {
  vm.$off();
}

function VeilEvents(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    $emit: $emit,
    $on: $on,
    $once: $once,
    $off: $off
  });
}

function $on(event, fn) {
  var vm = this;

  if (isArray(event)) event.forEach(function (event) {
    return vm.$on(event, fn);
  });else (vm._events[event] || (vm._events[event] = [])).push(fn);

  return vm;
}

function $once(event, fn) {
  var vm = this;

  function cb() {
    vm.$off(event, cb);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    fn.apply(vm, args);
  }

  cb.fn = fn;
  vm.$on(event, cb);

  return vm;
}

function $off(event, fn) {
  var vm = this;

  if (isUndefined(event) && isUndefined(fn)) initEvents(vm);

  if (isArray(event)) event.forEach(function (event) {
    return vm.$off(event, fn);
  });

  if (!fn) vm._events[event] = null;

  if (isFunction(fn)) {
    // TODO: research why we need to use a while loop to destruct events.
    // Using a foreach to remove events somehow mangles the array so that
    // not all events are properly handled.
    var cb = void 0,
        cbs = vm._events,
        i = vm._events.length;

    while (i--) {
      if (function (cb) {
        return cb === fn || cb.fn === fn;
      }) {
        cbs.splice(i, 1);
        break;
      }
    }
  }

  return vm;
}

function $emit(event) {
  for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
    args[_key2 - 1] = arguments[_key2];
  }

  var vm = this;

  vm._events[event] && vm._events[event].forEach(function (cb) {
    return cb.apply(vm, args);
  });

  return vm;
}

export { initEvents, destroyEvents, VeilEvents, $on, $once, $off, $emit };
