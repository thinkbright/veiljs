var asyncGenerator = function () {
  function AwaitValue(value) {
    this.value = value;
  }

  function AsyncGenerator(gen) {
    var front, back;

    function send(key, arg) {
      return new Promise(function (resolve, reject) {
        var request = {
          key: key,
          arg: arg,
          resolve: resolve,
          reject: reject,
          next: null
        };

        if (back) {
          back = back.next = request;
        } else {
          front = back = request;
          resume(key, arg);
        }
      });
    }

    function resume(key, arg) {
      try {
        var result = gen[key](arg);
        var value = result.value;

        if (value instanceof AwaitValue) {
          Promise.resolve(value.value).then(function (arg) {
            resume("next", arg);
          }, function (arg) {
            resume("throw", arg);
          });
        } else {
          settle(result.done ? "return" : "normal", result.value);
        }
      } catch (err) {
        settle("throw", err);
      }
    }

    function settle(type, value) {
      switch (type) {
        case "return":
          front.resolve({
            value: value,
            done: true
          });
          break;

        case "throw":
          front.reject(value);
          break;

        default:
          front.resolve({
            value: value,
            done: false
          });
          break;
      }

      front = front.next;

      if (front) {
        resume(front.key, front.arg);
      } else {
        back = null;
      }
    }

    this._invoke = send;

    if (typeof gen.return !== "function") {
      this.return = undefined;
    }
  }

  if (typeof Symbol === "function" && Symbol.asyncIterator) {
    AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
      return this;
    };
  }

  AsyncGenerator.prototype.next = function (arg) {
    return this._invoke("next", arg);
  };

  AsyncGenerator.prototype.throw = function (arg) {
    return this._invoke("throw", arg);
  };

  AsyncGenerator.prototype.return = function (arg) {
    return this._invoke("return", arg);
  };

  return {
    wrap: function (fn) {
      return function () {
        return new AsyncGenerator(fn.apply(this, arguments));
      };
    },
    await: function (value) {
      return new AwaitValue(value);
    }
  };
}();

















































var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

var isArray = Array.isArray;
var isFunction = function isFunction(value) {
  return typeof value === 'function';
};


var isUndefined = function isUndefined(value) {
  return typeof value === 'undefined';
};

var extend = !isFunction(Object.assign) ? function (target) {
  for (var _len = arguments.length, sources = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    sources[_key - 1] = arguments[_key];
  }

  return sources.reduceRight(function (result, source) {
    Object.keys(source).forEach(function (key) {
      result[key] = source[key];
    });

    return result;
  }, target);
} : Object.assign;


try {
  var options = Object.defineProperty({}, 'passive', {
    get: function get() {
      
    }
  });

  window.addEventListener('passive-test', null, options);
} catch (err) {}

var values = !isFunction(Object.values) ? function (obj) {
  return Object.keys(obj).map(function (key) {
    return obj[key];
  });
} : Object.values;

function VeilDOM(Veil) {
  Veil.prototype = extend(Veil.prototype, {
    $mount: $mount,
    $unmount: $unmount
  });

  Veil = extend(Veil, {
    attr: attr,
    query: query,
    queryAttr: queryAttr,
    remove: remove
  });
}

function $mount(node, target) {
  var vm = this;

  vm.$emit('mount', node, vm);
  target.appendChild(node);
}

function $unmount(nodes) {
  var vm = this;

  vm.$emit('unmount', nodes, vm);
  Veil.remove(nodes);
}

function attr(node, name, value) {
  return isUndefined(value) ? node.getAttribute(name) : node.setAttribute(name, value);
}

function query(query) {
  var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

  return [].concat(toConsumableArray(context.querySelectorAll(query)));
}

function queryAttr(name, context) {
  return query('[' + name.replace(':', '\\:') + ']', context);
}

function hasNode(n, nodes) {
  return nodes.some(function (node) {
    return n === node || node.contains(n);
  });
}

function remove(nodes) {
  (isArray(nodes) ? nodes : [nodes]).forEach(function (node) {
    return node.parentNode.removeChild(node);
  });
}

export { VeilDOM, $mount, $unmount, attr, query, queryAttr, hasNode, remove };
