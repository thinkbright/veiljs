var asyncGenerator = function () {
  function AwaitValue(value) {
    this.value = value;
  }

  function AsyncGenerator(gen) {
    var front, back;

    function send(key, arg) {
      return new Promise(function (resolve, reject) {
        var request = {
          key: key,
          arg: arg,
          resolve: resolve,
          reject: reject,
          next: null
        };

        if (back) {
          back = back.next = request;
        } else {
          front = back = request;
          resume(key, arg);
        }
      });
    }

    function resume(key, arg) {
      try {
        var result = gen[key](arg);
        var value = result.value;

        if (value instanceof AwaitValue) {
          Promise.resolve(value.value).then(function (arg) {
            resume("next", arg);
          }, function (arg) {
            resume("throw", arg);
          });
        } else {
          settle(result.done ? "return" : "normal", result.value);
        }
      } catch (err) {
        settle("throw", err);
      }
    }

    function settle(type, value) {
      switch (type) {
        case "return":
          front.resolve({
            value: value,
            done: true
          });
          break;

        case "throw":
          front.reject(value);
          break;

        default:
          front.resolve({
            value: value,
            done: false
          });
          break;
      }

      front = front.next;

      if (front) {
        resume(front.key, front.arg);
      } else {
        back = null;
      }
    }

    this._invoke = send;

    if (typeof gen.return !== "function") {
      this.return = undefined;
    }
  }

  if (typeof Symbol === "function" && Symbol.asyncIterator) {
    AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
      return this;
    };
  }

  AsyncGenerator.prototype.next = function (arg) {
    return this._invoke("next", arg);
  };

  AsyncGenerator.prototype.throw = function (arg) {
    return this._invoke("throw", arg);
  };

  AsyncGenerator.prototype.return = function (arg) {
    return this._invoke("return", arg);
  };

  return {
    wrap: function (fn) {
      return function () {
        return new AsyncGenerator(fn.apply(this, arguments));
      };
    },
    await: function (value) {
      return new AwaitValue(value);
    }
  };
}();

















































var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

var isFunction = function isFunction(value) {
  return typeof value === 'function';
};

var FILTERS = {
  currency: function currency(value) {
    var locale = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : navigator.language || navigator.userLanguage;

    var _currency = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'EUR';

    return !isNaN(parseFloat(value)) && parseFloat(value).toLocaleString(locale, {
      currency: _currency,
      style: 'currency'
    });
  },

  length: function length(value) {
    return typeof value !== 'undefined' && value.length;
  }
};

var parseFilter = function parseFilter(value, filter) {
  console.debug('parseFilter', value, filter);

  var parts = filter.split(':');
  var name = parts.shift();

  return name in FILTERS ? FILTERS[name].apply(FILTERS, [value].concat(toConsumableArray(parts))) : value && console.warn('Filter ' + name + ' is not defined.');
};

var registerFilter = function registerFilter(name, filterFn) {
  if (name in FILTERS) return console.warn('Filter ' + name + ' is already registered');

  return FILTERS[name] = filterFn;
};

var removeFilter = function removeFilter(name) {
  delete FILTERS[name];
};

var parseProp = function parseProp(prop, data) {
  console.debug('parseProp', prop, data);

  if (prop === '') return data;

  var parts = prop.split('|');
  var result = parts.shift().toString().trim().replace(/\[(\d+?)\]/g, '.$1').split('.').reduce(function (obj, key) {
    return isFunction(obj[key]) ? obj[key]() : obj[key];
  }, data);
  var filter = typeof parts[0] === 'string' && parts.shift().trim();

  return filter ? parseFilter(result, filter) : result;
};

export { FILTERS, parseFilter, registerFilter, removeFilter, parseProp };
